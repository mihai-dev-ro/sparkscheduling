package com.mihainicolla.SparkScheduling

import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Use this to test the app locally, from sbt:
  * sbt "run inputFile.txt outputFile.txt"
  *  (+ select CountingLocalApp when prompted)
  */
object CountingLocalApp extends App{
  val (secret, inputFile, outputFileWC, outputFileEncEmails) = (args(0), args(1), args(2), args(3))
  val conf = new SparkConf()
    .setMaster("local")
    .setAppName("Scheduling Experiment")

  Runner.run(conf, secret, inputFile, outputFileWC, outputFileEncEmails)
}

/**
  * Use this when submitting the app to a cluster with spark-submit
  * */
object CountingApp extends App{
  val (secret, inputFile, outputFileWC, outputFileEncEmails) = (args(0), args(1), args(2), args(3))

  // spark-submit command should supply all necessary config elements
  Runner.run(new SparkConf(), secret, inputFile, outputFileWC, outputFileEncEmails)
}

object Runner {
  def run(conf: SparkConf, 
    secret: String, 
    inputFile: String, 
    outputFileWC: String,
    outputFileEncEmails: String
  ): Unit = {

    conf.set("spark.scheduler.mode", "FAIR")
    var spark = SparkSession
      .builder
      .appName("Scheduling Experiment")
      .config(conf)
      .getOrCreate()
    var sc = spark.sparkContext

    // val stageMetrics = ch.cern.sparkmeasure.StageMetrics(spark) 
    // stageMetrics.begin()
    
    val rdd = sc.textFile(inputFile, 3)    
    
    val threadEncEmails = new Thread {
      override def run(): Unit = {
        val counts = PersonalInfoCount.withEmailEncrypted(rdd, secret)
        counts.saveAsTextFile(outputFileEncEmails)    
      }
    }
    threadEncEmails.start()

    val threadWC = new Thread {
      override def run(): Unit = {
        val counts2 = WordCount.withStopWordsFiltered(rdd)
        counts2.saveAsTextFile(outputFileWC)    
      }
    }
    threadWC.start()

    threadEncEmails.join()
    threadWC.join()

    // val counts = PersonalInfoCount.withEmailEncrypted(rdd, secret)
    // counts.saveAsTextFile(outputFileEncEmails)    
      

    // val counts2 = WordCount.withStopWordsFiltered(rdd)
    // counts2.saveAsTextFile(outputFileWC)    


    // stageMetrics.end()
    // stageMetrics.printReport()
  }
}
