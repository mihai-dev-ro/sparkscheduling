package com.mihainicolla.SparkScheduling

/**
 * Identify Personal Information and encrypt with a personal key
 */

import org.apache.spark.rdd._
import scala.util.matching.Regex
import org.apache.commons.codec.binary.Base64

import com.mihainicolla.crypto._
import com.mihainicolla.protocol.defaults._

object PersonalInfoCount {
	/**
	* The encrypter is identifying the personal information like email and 
	* encrypt the email and prefix the output with the key
	*/

	def encodeBase64(bytes: Array[Byte]) = Base64.encodeBase64String(bytes)

	def encryptText(plainText: String, secret: String) = encodeBase64(AES.encrypt(plainText, secret))

	def withEmailEncrypted(rdd: RDD[String], 		
		secret: String,
		separators : Array[Char] = "/".toCharArray
	): RDD[(String, Int)] = {
		
		val tokens: RDD[String] = rdd.flatMap(_.split(separators).
      map(_.trim.toLowerCase))
    
		val emailReg = new Regex("""^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$""")

    val emails: RDD[String] = tokens.map(token => (token, emailReg.unapplySeq(token).isDefined))
    	.filter(_._2)
    	.map {case (email, _) => encryptText(email, secret)}
    val emailsPairs = emails.map((_, 1))
    val emailsCounts = emailsPairs.reduceByKey(_ + _)
    emailsCounts
	}
	
}
